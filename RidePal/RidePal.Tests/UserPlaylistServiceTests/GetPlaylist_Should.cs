using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Services;
using RidePal.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace RidePal.Tests
{
    [TestClass]
    public class GetPlaylist_Should
    {
        [TestMethod]
        public async Task GetPlaylist_Should_ReturnCorrectPlaylistDTO_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetPlaylist_Should_ReturnCorrectPlaylistDTO_When_ParamsAreValid));

            var playlists = Utils.GetPlaylists();
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                var result = await sut.GetPlaylistAsync(1);

                //Assert
                Assert.AreEqual(1, result.Id);
                Assert.AreEqual("Playlist 1", result.Title);
                Assert.AreEqual(100, result.AverageRank);
                Assert.AreEqual(200, result.TotalPlaytime);
            }
        }

        [TestMethod]
        public async Task GetPlaylist_Should_Throw_When_PlaylistIsDeletedOrNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetPlaylist_Should_Throw_When_PlaylistIsDeletedOrNotFound));

            var playlists = Utils.GetPlaylists();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetPlaylistAsync(2));
            }
        }

        [TestMethod]
        public async Task GetPlaylist_Should_Throw_When_InvalidId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetPlaylist_Should_Throw_When_InvalidId));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.GetPlaylistAsync(0));
            }
        }
    }
}
