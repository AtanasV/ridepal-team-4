using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Services;
using RidePal.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests
{
    [TestClass]
    public class EditPlaylist_Should
    {
        [TestMethod]
        public async Task EditPlaylist_Should_ReturnTrueAndUpdateDb_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(EditPlaylist_Should_ReturnTrueAndUpdateDb_When_ParamsAreValid));

            var playlists = Utils.GetPlaylists();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                var result = await sut.EditPlaylistAsync(1, "New Name");

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual("New Name", actContext.Playlists.First().Title);
            }
        }

        [TestMethod]
        public async Task EditPlaylist_Should_ReturnFalse_When_PlaylistIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(EditPlaylist_Should_ReturnFalse_When_PlaylistIsDeleted));

            var playlists = Utils.GetPlaylists();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                var result = await sut.EditPlaylistAsync(2, "New Name");

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task EditPlaylist_Should_Throw_When_InvalidId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(EditPlaylist_Should_Throw_When_InvalidId));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.EditPlaylistAsync(0, "txt"));
            }
        }

        [TestMethod]
        public async Task EditPlaylist_Should_Throw_When_InvalidString()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(EditPlaylist_Should_Throw_When_InvalidString));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EditPlaylistAsync(1, "   "));
            }
        }

        [TestMethod]
        public async Task EditPlaylist_Should_Throw_When_NoSuchPlaylist()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(EditPlaylist_Should_Throw_When_NoSuchPlaylist));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var MapServiceMock = new Mock<IMapService>();

                var sut = new UserPlaylistService(actContext, MapServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.EditPlaylistAsync(1, "txt"));
            }
        }
    }
}
