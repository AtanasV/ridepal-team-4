﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using System;
using System.Threading.Tasks;

namespace RidePal.Tests.AdminServiceTests
{
    [TestClass]
    public class GetUserByName_Should
    {
        [TestMethod]
        public async Task ReturnsCorrectUserDTO_When_ParamsAreValid1()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnsCorrectUserDTO_When_ParamsAreValid1));
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                var result = await sut.GetUserByNameAsync("User 1");

                Assert.AreEqual(1, result.Id);
                Assert.AreEqual("User 1", result.Name);
            }
        }

        [TestMethod]
        public async Task GetUserByNameAsync_ShouldThrow_When_InvalidParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetUserByNameAsync_ShouldThrow_When_InvalidParameters));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetUserByNameAsync("  "));
            }
        }

        [TestMethod]
        public async Task GetUserAsync_ShouldThrow_When_NoSuchUSer()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetUserAsync_ShouldThrow_When_NoSuchUSer));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByNameAsync("No such name"));
            }
        }
    }
}
