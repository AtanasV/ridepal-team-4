﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.AdminServiceTests
{
    [TestClass]
    public class GetAllUsers_Should
    {
        [TestMethod]
        public async Task CorrectylReturnUsers_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectylReturnUsers_When_ParamsAreValid));
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }

            //Act and Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                var collection = await sut.GetAllUsersAsync();

                Assert.AreEqual(3, collection.Count());
            }
        }

        [TestMethod]
        public async Task Throw_When_NoUsersFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_NoUsersFound));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var store = new Mock<IUserStore<User>>();

                var sut = new AdminService(actContext);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllUsersAsync());
            }
        }
    }
}
