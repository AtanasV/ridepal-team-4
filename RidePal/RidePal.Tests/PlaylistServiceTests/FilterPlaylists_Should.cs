﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Services;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServiceTests
{
    [TestClass]
    public class FilterPlaylists_Should
    {
        [TestMethod]
        public async Task FilterPlaylist_WhenCriteriaIsName()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(FilterPlaylist_WhenCriteriaIsName));

            var tracks = Utils.GetTracks();
            var genres = Utils.GetGenres();
            var users = Utils.GetUsers();
            var playlists = Utils.GetPlaylists();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Tracks.AddRange(tracks);
                arrangeContext.Genres.AddRange(genres);
                arrangeContext.Users.AddRange(users);
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);
                var result = await sut.FilterPlaylistsAsync("name", "Playlist");

                //Assert
                Assert.AreEqual(result.Count(), 3);
                Assert.AreEqual("Playlist 1", result.First().Title);
            }
        }

        [TestMethod]
        public async Task FilterPlaylist_WhenCriteriaIsDuration()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(FilterPlaylist_WhenCriteriaIsDuration));

            var genres = Utils.GetGenres();
            var users = Utils.GetUsers();
            var playlists = Utils.GetPlaylists();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Genres.AddRange(genres);
                arrangeContext.Users.AddRange(users);
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);

                var result = await sut.FilterPlaylistsAsync("duration", "10");

                //Assert
                Assert.AreEqual(result.Count(), 2);
                Assert.AreEqual(result.First().Id, 3);
                Assert.AreEqual(result.Last().Id, 4);
            }
        }

        [TestMethod]
        public async Task FilterPlaylist_WhenCriteriaIsGenre()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(FilterPlaylist_WhenCriteriaIsGenre));

            var genres = Utils.GetGenres();
            var users = Utils.GetUsers();
            var playlists = Utils.GetPlaylists();
            var playlistGenres = Utils.GetPlaylistGenres();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Genres.AddRange(genres);
                arrangeContext.Users.AddRange(users);
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.PlaylistGenres.AddRange(playlistGenres);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);
                var result = await sut.FilterPlaylistsAsync("genre", "Genre 1");
                var result2 = await sut.FilterPlaylistsAsync("genre", "Genre 3");

                //Assert
                Assert.AreEqual(1, result.Count());
                Assert.AreEqual(2, result2.Count());
            }
        }

        [TestMethod]
        public async Task Return_When_CollectionIsEmpty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Return_When_CollectionIsEmpty));

            //Act & Assert
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);
                var result = await sut.FilterPlaylistsAsync("name", "aaaaaaa");

                Assert.AreEqual(0, result.Count());
            }
        }
    }
}
