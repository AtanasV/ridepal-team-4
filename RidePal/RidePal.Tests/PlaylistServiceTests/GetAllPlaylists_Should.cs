﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Services;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServiceTests
{
    [TestClass]
    public class GetAllPlaylists_Should
    {
        [TestMethod]
        public async Task GetPlaylists_When_DbIsFull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetPlaylists_When_DbIsFull));

            var genres = Utils.GetGenres();
            var playlists = Utils.GetPlaylists();
            var playlistGenres = Utils.GetPlaylistGenres();
            var users = Utils.GetUsers();

            using (var arrangeContext = new RidePalContext(options))
            {
                arrangeContext.Genres.AddRange(genres);
                arrangeContext.Users.AddRange(users);
                arrangeContext.Playlists.AddRange(playlists);
                arrangeContext.PlaylistGenres.AddRange(playlistGenres);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new RidePalContext(options))
            {
                var sut = new PlaylistService(actContext);

                var collection = await sut.GetAllPlaylistsAsync(5,1);

                Assert.AreEqual(3, collection.Count());

                Assert.AreEqual("Playlist 3", collection.First().Title);
                Assert.AreEqual(900, collection.First().TotalPlaytime);

                Assert.AreEqual("Playlist 4", collection.Skip(1).First().Title);
                Assert.AreEqual(800, collection.Skip(1).First().TotalPlaytime);

                Assert.AreEqual("Playlist 1", collection.Skip(2).First().Title);
                Assert.AreEqual(200, collection.Skip(2).First().TotalPlaytime);
                Assert.AreEqual(2, collection.Skip(2).First().Genres.Count());
                Assert.IsTrue(collection.Skip(2).First().Genres.Contains("Genre 1"));
                Assert.IsTrue(collection.Skip(2).First().Genres.Contains("Genre 3"));
                Assert.IsFalse(collection.Skip(2).First().Genres.Contains("Genre 2"));
            }
        }
    }
}

