﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Data.Models
{
    public class Track
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int DeezerAPIId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Link { get; set; }

        [Required]
        public int Duration { get; set; }

        [Required]
        public int Rank { get; set; }

        [Required]
        public string PreviewURL { get; set; }

        [Required]
        public Artist Artist { get; set; }
        public int ArtistId { get; set; }

        [Required]
        public Album Album { get; set; }
        public int AlbumId { get; set; }

        public ICollection<PlaylistTrack> PlaylistTracks { get; set; } = new List<PlaylistTrack>();
    }
}
