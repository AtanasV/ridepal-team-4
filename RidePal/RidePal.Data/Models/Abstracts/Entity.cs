﻿using System.ComponentModel.DataAnnotations;

namespace RidePal.Data.Models.Abstracts
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int DeezerAPIId { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
