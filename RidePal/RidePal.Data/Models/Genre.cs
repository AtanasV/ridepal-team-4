﻿using Newtonsoft.Json;
using RidePal.Data.Models.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Data.Models
{
    public  class Genre : Entity
    {
        [Required]
        public string URL { get; set; }

        public ICollection<PlaylistGenre> PlaylistGenres { get; set; } = new List<PlaylistGenre>();
    }
}
