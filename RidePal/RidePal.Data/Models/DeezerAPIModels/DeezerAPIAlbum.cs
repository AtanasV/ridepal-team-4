﻿namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPIAlbum
    {
        public int id { get; set; }

        public string title { get; set; }

        public string tracklist { get; set; }

        public DeezerAPIGenre genre { get; set; }
    }
}