﻿namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPITrack
    {
        public int id { get; set; }

        public string title { get; set; }

        public string link { get; set; }

        public int duration { get; set; }

        public int rank { get; set; }

        public string preview { get; set; }  
        
        public int artistId { get; set; }
        public DeezerAPIArtist artist { get; set; }

        public int albumId { get; set; }
        public DeezerAPIAlbum album { get; set; }
    }
}
