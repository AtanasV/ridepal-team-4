﻿namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPIGenre
    {
        public int id { get; set; }

        public string name { get; set; }

        public string picture{ get; set; }
    }
}