﻿using System.Collections.Generic;

namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPIGenresData
    {
        public ICollection<DeezerAPIGenre> data = new List<DeezerAPIGenre>();
    }
}
