﻿using System.Collections.Generic;

namespace RidePal.Data.Models.DeezerAPIModels
{
    public class DeezerAPIExtendedAlbum
    {
        public Genres genres { get; set; }
    }
    public class Genres
    {
        public ICollection<DeezerAPIGenre> data { get; set; } = new List<DeezerAPIGenre>();
    }
}
