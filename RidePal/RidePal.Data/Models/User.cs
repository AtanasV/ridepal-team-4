﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace RidePal.Data.Models
{
    public class User : IdentityUser<int>
    {
        [NotMapped]
        public string Token { get; set; }

        public bool IsDeleted { get; set; }

        [JsonIgnore]
        public ICollection<Playlist> Playlists { get; set; } = new List<Playlist>();
    }
}
