﻿using RidePal.Data.Models.DeezerAPIModels;

namespace RidePal.Data.Models.DeezerAPIMappers
{
    public static class DeezerAPIArtistMapper
    {
        public static Artist GetModel(this DeezerAPIArtist model)
        {
            var artist = new Artist
            {
                DeezerAPIId = model.id,
                Name = model.name,
                TrackListURL = model.tracklist
            };
            return artist;
        }
    }
}
