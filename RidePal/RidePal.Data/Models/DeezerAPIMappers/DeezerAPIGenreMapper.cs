﻿using RidePal.Data.Models.DeezerAPIModels;

namespace RidePal.Data.Models.DeezerAPIMappers
{
    public static class DeezerAPIGenreMapper
    {
        public static Genre GetModel(this DeezerAPIGenre model)
        {
            var genre = new Genre
            {
                DeezerAPIId = model.id,
                Name = model.name,
                URL = model.picture
            };
            return genre;
        }
    }
}
