﻿using RidePal.Data.Models.DeezerAPIModels;

namespace RidePal.Data.Models.DeezerAPIMappers
{
    public static class DeezerAPITrackMapper
    {
        public static Track GetModel(this DeezerAPITrack model)
        {
            var track = new Track
            {
                DeezerAPIId= model.id,
                Title = model.title,
                Link = model.link,
                Duration = model.duration,
                Rank = model.rank,
                PreviewURL = model.preview,
                ArtistId=model.artistId,
                AlbumId=model.albumId
            };
            return track;
        }
    }
}
