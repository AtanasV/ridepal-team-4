﻿using RidePal.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace RidePal.Data.Context
{
    public class RidePalContext : IdentityDbContext<User, Role, int>
    {
        public RidePalContext()
        {
        }

        public RidePalContext(DbContextOptions<RidePalContext> options)
            : base(options)
        {
        }

        public DbSet<Track> Tracks { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Playlist> Playlists { get; set; }
        public DbSet<PlaylistTrack> PlaylistTracks { get; set; }
        public DbSet<PlaylistGenre> PlaylistGenres { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // configure entity relations
            modelBuilder.Entity<PlaylistTrack>().HasKey(e => new { e.PlaylistId, e.TrackId });
            modelBuilder.Entity<PlaylistGenre>().HasKey(e => new { e.PlaylistId, e.GenreId });

           // modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }
    }
}
