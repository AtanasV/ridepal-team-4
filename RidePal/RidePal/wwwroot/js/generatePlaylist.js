﻿document.getElementById("validationMessage").style.color = "#dc3545"; //color of validation message

function handleSubmit(event) { //checks the total input 
    if (isSumCorrect()) {
        document.getElementById("validationMessage").innerHTML = "";
        return false;
    }
    event.preventDefault();
    document.getElementById("validationMessage").innerHTML = "Invalid input. Percentage sum must be 100";
}

function validateInput(event) {
    var x = parseInt(event.target.value) // checking each individual box first
    if (isNaN(x) || x < 0 || x > 100) {
        event.target.value = 0; 
    }
}

function isSumCorrect() {
    var x = parseInt(document.getElementById("number1").value) +
        parseInt(document.getElementById("number2").value) +
        parseInt(document.getElementById("number3").value) +
        parseInt(document.getElementById("number4").value) +
        parseInt(document.getElementById("number5").value);

    if (isNaN(x) || x < 100 || x > 100) {
        return false;
    } else {
        return true;
    }
}