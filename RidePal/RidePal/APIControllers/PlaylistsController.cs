﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Web.APIControllers
{
    /// <summary>
    /// The controller performs Read operations on playlist/playlists
    /// </summary>
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/playlists")]
    [ApiController]
    public class PlaylistsController : ControllerBase
    {
        private readonly IPlaylistService service;

        public PlaylistsController(IPlaylistService service)
        {
            this.service = service;
        }

        // GET: api/Playlists
        /// <summary>
        /// Returns all playlists.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="500">Oops! Can't retrieve playlists right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public async Task<ActionResult> GetAllPlaylists()
        {
            var collection= await service.GetAllPlaylistsAsync();
            return Ok(collection);
        }

        // GET: api/playlists/tracks/5
        /// <summary>
        /// Returns all tracks contained in a specific playlist.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid playlist id</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="404">Playlist is not found in DB</response>
        /// <response code="500">Oops! Can't retrieve tracks right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("tracks/id")]
        public async Task<ActionResult> GetTracks(int id)
        {
            IEnumerable<TrackDTO> collection = null;
            try
            {
                collection = await service.GetTracksAsync(id);
            }

            catch(ArgumentOutOfRangeException)
            {
                return BadRequest("Invalid input");
            }

            catch(ArgumentException)
            {
                return NotFound("Playlist doesn't exist");
            }

            return Ok(collection);
        }

        // GET: api/Playlists/filter?criteria=something&input=something
        /// <summary>
        /// Returns a filtered playlist collection or all playlists ordered by their rank descending.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="500">Oops! Can't retrieve playlists right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("filter")]
        public async Task<ActionResult> FilterPlaylists([FromQuery] string criteria, [FromQuery] string input)
        {
            if (string.IsNullOrWhiteSpace(criteria) || string.IsNullOrWhiteSpace(input))
            {
                return BadRequest();
            }
            return Ok(await service.FilterPlaylistsAsync(criteria, input));
        }


        // GET: api/playlists/5
        /// <summary>
        /// Returns a collection of TrackDTOs.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid playlist id</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="404">Playlist is not found in DB</response>
        /// <response code="500">Oops! Can't retrieve playlists right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<ActionResult> GetPlaylistPreview(int id)
        {
            IEnumerable<TrackDTO> collection = null;
            if (id < 1)
            {
                return BadRequest();
            }
            else
            {
                try
                {
                    collection = await service.ListenToPlaylistAsync(id);
                }

                catch (ArgumentException)
                {
                    return NotFound();
                }

                return Ok(collection);
            }
        }
    }
}
