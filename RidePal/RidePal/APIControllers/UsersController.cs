﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RidePal.Data.Models;
using RidePal.Services.Contracts;
using RidePal.Web.Models;
using System;
using System.Threading.Tasks;

namespace RidePal.Web.APIControllers
{
    /// <summary>
    /// The controller performs user authentication
    /// </summary>
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService service;

        public UsersController(IUserService service)
        {
            this.service = service;
        }

        //POST: api/users/authenticate
        /// <summary>
        /// Retrieves User with JWT Token included as property.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="500">Oops! Can't retrieve the user right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("authenticate")]
        [AllowAnonymous]
        public async Task<IActionResult> Authenticate([FromBody] LoginCredentialsModel model)
        {
            User user = null;
            try
            {
                user = await this.service.Authenticate(model.Username, model.Password);
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }

            return Ok(user);
        }
    }
}
