﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RidePal.Data.Models;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using RidePal.Web.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Web.APIControllers
{
    /// <summary>
    /// The controller performs CRUD operations on playlist/playlists by registered user
    /// </summary>
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/userplaylist")]
    [ApiController]
    public class UserPlaylistController : ControllerBase
    {
        private readonly IUserPlaylistService service;
        private readonly UserManager<User> userManager;

        public UserPlaylistController(IUserPlaylistService service, UserManager<User> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }

        //GET: api/userplaylist/5
        /// <summary>
        /// Retrieves a playlists from DB by specified id.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="404">No playlists found in DB</response>
        /// <response code="500">Oops! Can't list the playlists right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            PlaylistDTO playlist = null;
            try
            {
                playlist = await this.service.GetPlaylistAsync(id);
            }
            catch (Exception)
            {
                return NotFound("Playlist not found");
            }

            return Ok(playlist);
        }

        //GET: api/userplaylist/user/5
        /// <summary>
        /// Retrieves all playlists for the specified user.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="404">User not found in DB</response>
        /// <response code="500">Oops! Can't retrieve the playlists right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetForUser(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            IEnumerable<PlaylistDTO> playlists = null;
            try
            {
                playlists = await this.service.GetPlaylistsForUserAsync(id);
            }
            catch (Exception)
            {
                return NotFound("User not found");
            }

            return Ok(playlists);
        }

        //GET: api/userplaylist/count/5
        /// <summary>
        /// Retrieves the count of playlists for the specified user.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="404">User not found in DB</response>
        /// <response code="500">Oops! Can't retrieve the count right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("count/{id}")]
        public async Task<IActionResult> GetCountForUser(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }

            int count;
            try
            {
                count = await this.service.GetPlaylistsForUserCountAsync(id);
            }
            catch (Exception)
            {
                return NotFound("User not found");
            }
            return Ok(count);
        }

        // PUT: api/userplaylist/5?name=somenamehere
        /// <summary>
        /// Edits the title of a playlist.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="404">Playlist not found in DB</response>
        /// <response code="500">Oops! Can't retrieve the playlist right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromQuery] string name)
        {
            if (id < 1 || string.IsNullOrWhiteSpace(name))
            {
                return BadRequest("Invalid input");
            }

            bool result = false;
            try
            {
                result = await this.service.EditPlaylistAsync(id, name);
            }
            catch (Exception)
            {
                return NotFound("Playlist not found");
            }

            if (result == true)
            {
                return Ok();
            }

            return NotFound("Update failed");
        }

        // POST: api/userplaylist
        /// <summary>
        /// Generates playlist for user.
        /// </summary>
        /// <response code="201">Playlist succesfully created</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="500">Oops! Can't generate the playlist right now</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("")]
        public async Task<ActionResult> Post([FromBody] GeneratePlaylistViewModel model)
        {
            if (model == null)
            {
                return BadRequest("Invalid input");
            }

            var tuple1 = Tuple.Create("Rock", model.Percentage1);
            var tuple2 = Tuple.Create("Metal", model.Percentage2);
            var tuple3 = Tuple.Create("Jazz", model.Percentage3);
            var tuple4 = Tuple.Create("Classical", model.Percentage4);
            var tuple5 = Tuple.Create("Rap/Hip Hop", model.Percentage5);
            List<Tuple<string, int>> tupleList = new List<Tuple<string, int>> { tuple1, tuple2, tuple3, tuple4, tuple5 };

            var userId = int.Parse(this.userManager.GetUserId(this.User));
            PlaylistDTO playlist = null;
            try
            {
                playlist = await this.service.GeneratePlaylistAsync(tupleList, model.StartPoint,
                    model.EndPoint, model.Title, userId);
            }
            catch (Exception)
            {
                return BadRequest();
            }

            return Created("", playlist);
        }

        // DELETE: api/userplaylist/1
        /// <summary>
        /// Deletes playlist.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">Request is available only for authorized/registered users</response>
        /// <response code="404">Playlist not found in DB</response>
        /// <response code="500">Oops! Can't retrieve the playlist right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePlaylist(int id)
        {
            if (id < 1)
            {
                return BadRequest("Invalid input");
            }

            bool result = false;
            try
            {
                result = await this.service.DeletePlaylistAsync(id);
            }
            catch (Exception)
            {
                return NotFound();
            }

            if (result == true)
            {
                return Ok();
            }
            else
            {
                return NotFound("Deletion failed");
            }
        }
    }
}
