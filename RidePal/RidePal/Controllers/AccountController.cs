﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Web.Models;
using System;
using System.Threading.Tasks;

namespace RidePal.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IAdminService adminService;
        private readonly ILogger<AccountController> logger;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IAdminService adminService, 
            ILogger<AccountController> logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.adminService = adminService ?? throw new ArgumentNullException(nameof(adminService));
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult Register()
        {
            var vm = new RegisterViewModel();
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            UserDTO userDTO = null;
            try
            {
                userDTO = await adminService.GetUserByNameAsync(model.UserName);

                ModelState.AddModelError("", "User with same name allready exist.");
                return View(model);
            }
            catch (Exception)
            {
                if (ModelState.IsValid)
                {
                    var user = new User { UserName = model.UserName, Email = model.Email };
                    var result = await this.userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var result2 = await userManager.AddToRoleAsync(user, "User");
                        if (result2.Succeeded)
                        {
                            await this.signInManager.SignInAsync(user, isPersistent: false);
                            logger.LogInformation("Account with name: {0} was created.", model.UserName);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }

                return RedirectToAction("Error", "Home");
            }
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            UserDTO userDTO = null;
            try
            {
                userDTO = await adminService.GetUserByNameAsync(model.UserName);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "User does not exist. Go to Register.");
                return View(model);
            }

            if (ModelState.IsValid)
            {
                if (userDTO.IsDeleted == false)
                {
                    var result = await this.signInManager.PasswordSignInAsync(model.UserName, model.Password, isPersistent: true, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        logger.LogInformation("User with name: {0} logged in.", model.UserName);
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "You are trying to access a deactivated account.");
                    return View(model);
                }
            }

            ModelState.AddModelError("", "Invalid login attempt.");
            return View(model);
        }
    }
}
