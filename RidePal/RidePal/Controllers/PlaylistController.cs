﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using RidePal.Web.Models;

namespace RidePal.Web.Controllers
{
    public class PlaylistController : Controller
    {
        private readonly IPlaylistService playlistService;
        public PlaylistController(IPlaylistService playlistService)
        {
            this.playlistService = playlistService ?? throw new ArgumentNullException(nameof(playlistService));
        }

        public async Task<IActionResult> Index(int? itemsCount, int? pageNumber)
        {
            ViewBag.playlistCount = await playlistService.GetPlaylistCountAsync();

            IEnumerable<PlaylistDTO> playlists = null;
            try
            {
                playlists = await playlistService.GetAllPlaylistsAsync(itemsCount.HasValue ? itemsCount.Value : 5, pageNumber.HasValue ? pageNumber.Value : 1);
            }
            catch (ArgumentException)
            {
                return View("Views/Home/NoContent.cshtml");
            }

            var collection = playlists.Select(playlist => new PlaylistViewModel(playlist)).ToList();
            return View(collection);
        }

        public async Task<JsonResult> FilterOnKeyUp(string criteria, string input, int pageNum)
        {
            var collection = await playlistService.FilterPlaylistsAsync(criteria, input);
            int count = playlistService.GetFilterPlaylistCountAsync(collection);

            var filteredCollection = playlistService.FilterPlaylistPaging(collection, pageIndex: pageNum);
            var viewModels = filteredCollection.Select(p => new PlaylistViewModel(p)).ToList();

            var jsonModel = new JSonPlaylistViewModel { Models = viewModels, Count = count }; // response.models

            return Json(jsonModel);
        }

        public async Task<JsonResult> DetailsJson(int id, int? itemsCount, int pageNum)
        {
            int count = await playlistService.GetTracksCountAsync(id);
            var tracks = await playlistService.GetTracksAsync(id, pageIndex: pageNum);

            var viewModels = tracks.Select(t => new TrackViewModel(t)).ToList();
            var jsonModel = new JsonTrackViewModel { Models = viewModels, Count = count };

            return Json(jsonModel);
        }

        public async Task<IActionResult> Details(int id, int? itemsCount, int? pageNumber)
        {
            if (id < 1)
            {
                return View("Views/Home/BadRequest.cshtml");
            }

            ViewBag.tracksCount = await playlistService.GetTracksCountAsync(id);
            IEnumerable<TrackDTO> tracks = null;
            try
            {
                tracks = await playlistService.GetTracksAsync(id, itemsCount.HasValue ? itemsCount.Value : 5, pageNumber.HasValue ? pageNumber.Value : 1);
            }
            catch (Exception)
            {
                return View("Views/Home/NoContent.cshtml");
            }

            var collection = tracks.Select(t => new TrackViewModel(t));
            return View(collection);
        }

        public async Task<IActionResult> Listen(int id)
        {
            if (id < 1)
            {
                return View("Views/Home/BadRequest.cshtml");
            }

            IEnumerable<TrackDTO> playlistPreview = null;
            try
            {
                playlistPreview = await playlistService.ListenToPlaylistAsync(id);
            }
            catch (Exception)
            {
                return View("Views/Home/NoContent.cshtml");
            }

            var collection = playlistPreview.Select(x => new TrackViewModel(x)).ToList();
            return View(collection);
        }
    }
}
