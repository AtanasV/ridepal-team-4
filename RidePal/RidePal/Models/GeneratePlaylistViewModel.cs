﻿using System.ComponentModel.DataAnnotations;

namespace RidePal.Web.Models
{
    public class GeneratePlaylistViewModel
    {
        public int Percentage1 { get; set; }
        public int Percentage3 { get; set; }
        public int Percentage2 { get; set; }
        public int Percentage4 { get; set; }
        public int Percentage5 { get; set; }

        [Required]
        [MinLength(3), MaxLength(128)]
        public string StartPoint { get; set; }

        [Required]
        [MinLength(3), MaxLength(128)]
        public string EndPoint { get; set; }

        [Required]
        [MinLength(3), MaxLength(128)]
        public string Title { get; set; }
    }
}
