﻿using RidePal.Services.DTOs;
using System;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Web.Models
{
    public class PlaylistViewModel
    {
        public PlaylistViewModel()
        {
        }

        public PlaylistViewModel(PlaylistDTO model)
        {
            Id = model.Id;
            Title = model.Title;
            AverageRank = model.AverageRank;
            TotalDuration = Convert.ToInt32(model.TotalPlaytime/60);
            Genres = string.Join(", ", model.Genres);
        }

        [Required]
        public int Id { get; set; }

        [Required]
        [MinLength(3), MaxLength(128)]
        public string Title { get; set; }

        [Required]
        public int AverageRank { get; set; }

        [Required]
        public int TotalDuration { get; set; }

        public bool IsDeleted { get; set; }

        public string Genres { get; set; }
    }
}
