﻿using RidePal.Services.DTOs;

namespace RidePal.Web.Models
{
    public class TrackViewModel
    {
        public TrackViewModel()
        {
        }

        public TrackViewModel(TrackDTO dto)
        {
            Id = dto.Id;
            Title = dto.Title;
            ArtistName = dto.ArtistName;
            PreviewURL = dto.PreviewURL;
            DurationMinutes = dto.Duration / 60;
            DurationSeconds = dto.Duration % 60;
        }

        public int Id { get; set; }

        public string Title { get; set; }  
        
        public string ArtistName { get; set; }

        public string PreviewURL { get; set; }

        public int DurationMinutes { get; set; }

        public int DurationSeconds { get; set; }
    }
}