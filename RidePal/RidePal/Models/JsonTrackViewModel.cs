﻿using System.Collections.Generic;

namespace RidePal.Web.Models
{
    public class JsonTrackViewModel
    {
        public ICollection<TrackViewModel> Models = new List<TrackViewModel>();

        public int Count { get; set; }
    }
}
