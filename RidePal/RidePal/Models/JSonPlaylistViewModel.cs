﻿using System.Collections.Generic;

namespace RidePal.Web.Models
{
    public class JSonPlaylistViewModel
    {
        public ICollection<PlaylistViewModel> Models = new List<PlaylistViewModel>();

        public int Count { get; set; }
    }
}
