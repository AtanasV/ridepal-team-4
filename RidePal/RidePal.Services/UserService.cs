using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RidePal.Data.Models;
using RidePal.Services.Contracts;
using RidePal.Services.Helpers;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class UserService : IUserService
    {
        private readonly AppSettings appSettings;
        private readonly SignInManager<User> signInManager;
        private readonly UserManager<User> userManager;

        public UserService(IOptions<AppSettings> appSettings, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            this.appSettings = appSettings.Value;
            this.signInManager = signInManager;
            this.userManager = userManager;
        }

        /// <summary>
        /// Returns User with JWT Token included as property.
        /// </summary>
        /// <param name="username">Name of user for authentication.</param>
        /// <param name="password">Password of user for authentication.</param>
        /// <returns>User or null if no user found in DB with specified name.</returns>
        /// <exception cref="System.ArgumentException">Throws if sign-in was unsuccessful.</exception>
        public async Task<User> Authenticate(string username, string password)
        {
            var user = await userManager.FindByNameAsync(username);
            var signInResult = await signInManager.CheckPasswordSignInAsync(user, password, false);
            if (signInResult != SignInResult.Success)
            {
                throw new ArgumentException();
            }

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return user;
        }
    }
}