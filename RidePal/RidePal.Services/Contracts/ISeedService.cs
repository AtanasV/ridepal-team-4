﻿using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface ISeedService
    {
        Task GetDataFromExternalAPIAsync();
    }
}
