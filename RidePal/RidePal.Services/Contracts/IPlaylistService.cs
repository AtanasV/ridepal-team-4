﻿using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IPlaylistService
    {
        Task<IEnumerable<PlaylistDTO>> FilterPlaylistsAsync(string criteria, string input);

        IEnumerable<PlaylistDTO> FilterPlaylistPaging(IEnumerable<PlaylistDTO> collection, int itemsPerPage = 5, int pageIndex = 1);

        Task<IEnumerable<PlaylistDTO>> GetAllPlaylistsAsync(int iremsCount=5,int pageIndex = 1);

        int GetFilterPlaylistCountAsync(IEnumerable<PlaylistDTO> collection);

        Task<int> GetPlaylistCountAsync();

        Task<int> GetTracksCountAsync(int playlistId);

        Task<IEnumerable<TrackDTO>> GetTracksAsync(int id, int itemsPerPage = 5, int pageIndex = 1);

        Task<IEnumerable<TrackDTO>> ListenToPlaylistAsync(int playlistId);
    }
}

