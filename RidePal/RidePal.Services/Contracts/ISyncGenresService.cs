﻿using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface ISyncGenresService
    {
        Task SyncGenresAsync();
    }
}
