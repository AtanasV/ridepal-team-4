﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public  interface ICollectPlaylistsService
    {
        Task<List<long>> GetPlaylistsFromChartsAsync();
    }
}
