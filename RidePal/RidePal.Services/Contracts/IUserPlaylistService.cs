﻿using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IUserPlaylistService
    {
        Task<PlaylistDTO> GeneratePlaylistAsync(List<Tuple<string, int>> genres, string startPoint, string endPoint, string title, int userId);
        
        Task<bool> EditPlaylistAsync(int playlistId, string title);

        Task<bool> DeletePlaylistAsync(int playlistId);

        Task<PlaylistDTO> GetPlaylistAsync(int playlistId);

        Task<IEnumerable<PlaylistDTO>> GetPlaylistsForUserAsync(int userId,int itemsPerPage=5,int pageIndex=1);

        Task<int> GetPlaylistsForUserCountAsync(int userId);
    }
}
