﻿using RidePal.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace RidePal.Services.DTOMappers
{
    public static  class UserDTOMapper
    {
        public static UserDTO GetModel(this User model)
        {
            var dto = new UserDTO
            {
                Id = model.Id,
                Name = model.UserName,
                Email=model.Email,
                Playlists = model.Playlists.GetModel(),
                IsDeleted=model.IsDeleted
            };
            return dto;
        }
        public static IEnumerable<UserDTO> GetModel(this ICollection<User> items)
        {
            return items.Select(GetModel).ToList();
        }
    }
}
