﻿using RidePal.Data.Models;
using RidePal.Services.DTOs;

namespace RidePal.Services.DTOMappers
{
    public static class GenreDTOMapper
    {
        public static GenreDTO GetModel(this Genre model)
        {
            var genre = new GenreDTO
            {
                Id = model.Id,
                Name = model.Name,
                URL = model.URL
            };
            return genre;
        }
    }
}
