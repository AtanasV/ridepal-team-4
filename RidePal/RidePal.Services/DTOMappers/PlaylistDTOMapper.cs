﻿using RidePal.Data.Models;
using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace RidePal.Services.DTOMappers
{
    public static class PlaylistTDTOMapper
    {
        public static PlaylistDTO GetModel(this Playlist model)
        {

            var dto = new PlaylistDTO
            {
                Id = model.Id,
                Title = model.Title,
                TotalPlaytime = model.TotalPlaytime,
                AverageRank = model.Rank,
                UserId = model.UserId
            };
          
            return dto;
        }
      
        public static IEnumerable<PlaylistDTO> GetModel(this ICollection<Playlist> items)
        {
            return items.Select(playlist=>playlist.GetModel()).ToList();
        }
    }
}
