﻿using Microsoft.EntityFrameworkCore;
using RidePal.Data.Context;
using RidePal.Services.Contracts;
using RidePal.Services.DTOMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class AdminService : IAdminService
    {
        private readonly RidePalContext dbContext;

        public AdminService(RidePalContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Returns a collection of all users in DataBase.
        /// </summary>
        /// <returns>A collection of UserDTOs</returns>
        /// <exception cref="System.ArgumentException">Throws if no users found in DB.</exception>
        public async Task<IEnumerable<UserDTO>> GetAllUsersAsync()
        {
            var collection = await dbContext.Users.ToListAsync();

            if (collection.Count == 0)
            {
                throw new ArgumentException("No user found");
            }

            return collection.GetModel();
        }

        /// <summary>
        /// Returns the UserDTO for user with specified id.
        /// </summary>
        /// <param name="id">The id of the user to look for.</param>
        /// <returns>UserDTO</returns>
        /// <exception cref="System.ArgumentException">Throws if no user found in DB with such id</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if input is invalid.</exception>
        public async Task<UserDTO> GetUserAsync(int id)
        {
            if (id < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            var user = await this.dbContext.Users
                .Where(user => user.Id == id)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new ArgumentException("No user found");
            }

            var userDTO = user.GetModel();
            return userDTO;
        }

        /// <summary>
        /// Returns the UserDTO for user with specified name.
        /// </summary>
        /// <param name="name">The user name to look for.</param>
        /// <returns>UserDTO</returns>
        /// <exception cref="System.ArgumentException">Throws if no user found in DB with such name.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if input is invalid.</exception>
        public async Task<UserDTO> GetUserByNameAsync(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException();
            }

            var user = await this.dbContext.Users
                .Where(user => user.UserName == name)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new ArgumentException("No user found");
            }

            var userDTO = user.GetModel();
            return userDTO;
        }

        /// <summary>
        /// Edits the name of user with specified id.
        /// </summary>
        /// <param name="userId">The id of the user to be edited.</param>
        /// <param name="name">The new name to be applied.</param>
        /// <returns>True if user name was edited. False if no user found in DB with such id, or user was deleted.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if invalid id input.</exception>
        /// <exception cref="System.ArgumentNullException">Throws if invalid name input.</exception>
        public async Task<bool> EditUserAsync(int userId, string name)
        {
            if (userId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException();
            }

            var user = await dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId && x.IsDeleted == false);
            if (user == null)
            {
                return false;
            }

            user.UserName = name;
            user.NormalizedUserName = name.ToUpper();
            await dbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Deletes the user with specified id (IsDeleted property for user is set to "true").
        /// </summary>
        /// <param name="userId">The id of the user to be deleted.</param>
        /// <returns>True if user was deleted. False if user is present in DB, but is already deleted.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if invalid id input.</exception>
        /// <exception cref="System.ArgumentException">Throws if no user found in DB with specified id.</exception>
        public async Task<bool> DeleteUserAsync(int userId)
        {
            if (userId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            var user = await this.dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null)
            {
                throw new ArgumentException();
            }
            if (user.IsDeleted == true)
            {
                return false;
            }

            user.IsDeleted = true;
            await dbContext.SaveChangesAsync();

            return true;
        }
    }
}
