﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services.Contracts;
using RidePal.Services.DTOMappers;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class UserPlaylistService : IUserPlaylistService
    {
        private readonly RidePalContext dbContext;
        private readonly IMapService mapService;

        public UserPlaylistService(RidePalContext dbContext, IMapService mapService)
        {
            this.dbContext = dbContext;
            this.mapService = mapService ?? throw new ArgumentNullException(nameof(mapService));
        }

        /// <summary>
        /// Generates a playlist and saves it in DB.
        /// </summary>
        /// <param name="genres">Collection of genre/percentage tuples.</param>
        /// <param name="startPoint">Name of town for start point.</param>
        /// <param name="endPoint">Name of town for end point.</param>
        /// <param name="title">Title for the playlist.</param>
        /// <param name="userId">Id of the user who generates the playlist.</param>
        /// <returns>PlaylistDTO with included playlist Title, AverageRank based on each track's rank,
        /// TotalPlaytime based on calculated travel time by car from start to end point +/- 5minutes,
        /// a collection of genre/percentage of each genre included, Name of playlist owner.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Throws if invalid input.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if calculated playlist duration based on start and end point is less than 1 sec.</exception>
        public async Task<PlaylistDTO> GeneratePlaylistAsync(List<Tuple<string, int>> genres, string startPoint, string endPoint, string title, int userId)
        {
            if (genres == null || genres.Count() == 0 ||
                string.IsNullOrWhiteSpace(startPoint) || string.IsNullOrWhiteSpace(endPoint) || string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentNullException();
            }

            int playlistDuration = await mapService.GetTravelTimeAsync(startPoint, endPoint);

            var playlist = new Playlist()
            {
                Title = title,
                UserId = userId
            };

            int totalDuration = 0;
            IEnumerable<int> trackIdsUsed = FillPlaylist(ref totalDuration, genres, playlistDuration);
            playlist.TotalPlaytime = totalDuration;

            await this.dbContext.Playlists.AddAsync(playlist);
            await dbContext.SaveChangesAsync();

            foreach (var item in genres)
            {
                var tempGenre = this.dbContext.Genres.First(g => g.Name == item.Item1);
                await this.dbContext.PlaylistGenres.AddAsync(new PlaylistGenre { PlaylistId = playlist.Id, GenreId = tempGenre.Id });
            }
            await dbContext.SaveChangesAsync();

            foreach (var item in trackIdsUsed)
            {
                await this.dbContext.PlaylistTracks.AddAsync(new PlaylistTrack { TrackId = item, PlaylistId = playlist.Id });
            }
            await this.dbContext.SaveChangesAsync();

            playlist.Rank = GetAverageRank(playlist.Id);
            await this.dbContext.SaveChangesAsync();

            return playlist.GetModel();
        }

        /// <summary>
        /// Edits the title of a playlist.
        /// </summary>
        /// <param name="playlistId">The id of the playlist to be edited.</param>
        /// <param name="title">The new title to be applied.</param>
        /// <returns>True if playlist was edited successfully. False if playlist is present in DB, but deleted.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if invalid id input.</exception>
        /// <exception cref="System.ArgumentNullException">Throws if invalid title input.</exception>
        /// <exception cref="System.ArgumentException">Throws if playlist with specified id was not found in DB.</exception>
        public async Task<bool> EditPlaylistAsync(int playlistId, string title)
        {
            if (playlistId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentNullException();
            }

            var playlist = await this.dbContext.Playlists.FirstOrDefaultAsync(p => p.Id == playlistId);
            if (playlist == null)
            {
                throw new ArgumentException();
            }
            if (playlist.IsDeleted == true)
            {
                return false;
            }

            playlist.Title = title;
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Deletes the playlist with specified id (IsDeleted property for playlist is set to "true").
        /// </summary>
        /// <param name="playlistId">The id of the playlist to be deleted.</param>
        /// <returns>True if playlist was deleted. False if playlist is present in DB, but is already deleted.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if invalid id input.</exception>
        /// <exception cref="System.ArgumentException">Throws if no playlist found in DB with specified id.</exception>
        public async Task<bool> DeletePlaylistAsync(int playlistId)
        {
            if (playlistId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            var playlist = await this.dbContext.Playlists.FirstOrDefaultAsync(p => p.Id == playlistId);
            if (playlist == null)
            {
                throw new ArgumentException();
            }
            if (playlist.IsDeleted == true)
            {
                return false;
            }

            playlist.IsDeleted = true;
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Returns PlaylistDTO for playlist with specified id.
        /// </summary>
        /// <param name="playlistId">The id of the playlist to look for.</param>
        /// <returns>PlaylistDTO</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if input is invalid.</exception>
        /// <exception cref="System.ArgumentException">Throws if no playlist found in DB with such id.</exception>
        public async Task<PlaylistDTO> GetPlaylistAsync(int playlistId)
        {
            if (playlistId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            var playlist = await this.dbContext.Playlists
                .Include(playlist => playlist.User)
                .Where(playlist => playlist.Id == playlistId)
                .Where(playlist => playlist.IsDeleted == false)
                .FirstOrDefaultAsync();

            if (playlist == null)
            {
                throw new ArgumentException("Playlist not found");
            }
            return playlist.GetModel();
        }

        /// <summary>
        /// Returns a collection of PlaylistDTOs for playlists of a specified user.
        /// </summary>
        /// <param name="userId">The id of the user.</param>
        /// <param name="itemsPerPage">The number of playlists per page.</param>
        /// <param name="pageIndex">The index of current page.</param>
        /// <returns>Collection of PlaylistDTOs</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if invalid id input.</exception>
        /// /// <exception cref="System.ArgumentException">Throws if no user found in DB with such id.</exception>
        public async Task<IEnumerable<PlaylistDTO>> GetPlaylistsForUserAsync(int userId, int itemsPerPage = 5, int pageIndex = 1)
        {
            if (userId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (!this.dbContext.Users.Any(u => u.Id == userId))
            {
                throw new ArgumentException();
            }

            var collection = await dbContext.Playlists
                    .Include(p => p.User)
                    .Where(p => p.UserId == userId)
                    .Where(p => p.IsDeleted == false)
                    .OrderByDescending(playlist => playlist.Rank)
                    .Skip((pageIndex - 1) * itemsPerPage)
                    .Take(itemsPerPage)
                    .ToListAsync();

            IEnumerable<PlaylistDTO> playlistDTOs = collection.GetModel();
            foreach (var playlist in playlistDTOs)
            {
                var tempGenres = await dbContext.PlaylistGenres
                     .Include(p => p.Genre)
                     .Where(p => p.PlaylistId == playlist.Id)
                     .Select(pg => pg.Genre.Name).ToListAsync();
                playlist.Genres = tempGenres;
            }
            return playlistDTOs;
        }

        /// <summary>
        /// Returns the count of playlists for specified user.
        /// </summary>
        /// <param name="userId">The id of the user.</param>
        /// <returns>An integer of playlists' count.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if invalid id input.</exception>
        public async Task<int> GetPlaylistsForUserCountAsync(int userId)
        {
            if (userId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (!this.dbContext.Users.Any(u => u.Id == userId))
            {
                throw new ArgumentException();
            }

            return await dbContext.Playlists
                        .Include(p => p.User)
                        .Where(p => p.UserId == userId)
                        .Where(p => p.IsDeleted == false)
                        .CountAsync();
        }

        /// <summary>
        /// Returns the playlist's rank, calculated as average of the tracks' ranks.
        /// </summary>
        /// <param name="playlistId">The id of the playlist.</param>
        /// <returns>An integer of the playlist's average rank.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if invalid id input.</exception>
        /// <exception cref="System.ArgumentException">Throws if no playlist found in DB with such id.</exception>
        private int GetAverageRank(int playlistId)
        {
            if (playlistId < 1)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (!this.dbContext.Playlists.Any(pl => pl.Id == playlistId))
            {
                throw new ArgumentException();
            }

            int rank = (int)Math.Round(this.dbContext.PlaylistTracks
                .Include(pl => pl.Track)
                .Where(p => p.PlaylistId == playlistId)
                .Average(p => p.Track.Rank));
            return rank;
        }

        /// <summary>
        /// Returns a collection of ids of tracks which were included in playlist and calculates the total duration
        /// of generated playlist with range +/- 5 minutes from the duration, calculated by start and end point of travel.
        /// </summary>
        /// <param name="totalDuration">The acumulated duration of playlist, based on included tracks.</param>
        /// <param name="genres">Collection of genre/percentage tuples, by which tracks are to be chosen from DB.</param>
        /// <param name="playlistDuration">The calculated travel time by car from start to end point.</param>
        /// <returns>Collection of the ids of all tracks in playlist.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if invalid duration input.</exception>
        private IEnumerable<int> FillPlaylist(ref int totalDuration, List<Tuple<string, int>> genres, int playlistDuration)
        {
            if (playlistDuration < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            int totalDurationTemp = 0;
            int index = 0;

            var trackIdsUsed = new List<int>();
            for (int i = 0; i < genres.Count; i++)
            {
                string genre = genres[i].Item1;
                int percentage = genres[i].Item2;

                var tracksIdsList = this.dbContext.Tracks
                    .Where(t => t.Album.Genre.Name == genre)
                    .Select(t => t.Id).ToList();

                var trackArtistIdsList = new List<int>();
                var shuffledIdsList = tracksIdsList.OrderBy(a => Guid.NewGuid()).ToList();

                while (totalDurationTemp <= playlistDuration * percentage * 0.01)
                {
                    var tempTrack = this.dbContext.Tracks.First(t => t.Id == shuffledIdsList[index]);

                    if (!trackArtistIdsList.Contains(tempTrack.ArtistId))
                    {
                        totalDuration += tempTrack.Duration;
                        totalDurationTemp += tempTrack.Duration;
                        trackIdsUsed.Add(tempTrack.Id);
                        trackArtistIdsList.Add(tempTrack.ArtistId);
                    }
                    index++;
                }
                totalDurationTemp = 0;
                index = 0;
            }
            return trackIdsUsed;
        }
    }
}
