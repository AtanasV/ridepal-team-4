﻿using RidePal.Services.DTOs;
using System.Collections.Generic;

namespace RidePal.Services
{
    public class UserDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public int RoleId { get; set; }

        public bool IsDeleted { get; set; }

        public IEnumerable<PlaylistDTO> Playlists = new List<PlaylistDTO>();
    }
}