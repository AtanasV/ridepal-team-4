﻿using System.Collections.Generic;

namespace RidePal.Services.DTOs
{
    public class PlaylistDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string UserName { get; set; }

        public int UserId { get; set; }

        public int TotalPlaytime { get; set; }

        public int AverageRank { get; set; }

        public bool IsDeleted { get; set; }

        public IEnumerable<string> Genres { get; set; } = new List<string>();
    }
}
