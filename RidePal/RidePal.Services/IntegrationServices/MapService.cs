﻿using Newtonsoft.Json;
using RidePal.Data.Models.BingAPIModels;
using RidePal.Services.Contracts;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RidePal.Services.IntegrationServices
{
    public class MapService : IMapService
    {
        /// <summary>
        /// Fetches from BingMaps external API the travel time in seconds from start to end point of travel.
        /// </summary>
        /// <param name="wp0">Name of town for start point.</param>
        /// <param name="wp1">Name of town for end point.</param>
        /// <returns>An integer of calculated time in seconds.</returns>
        public async Task<int> GetTravelTimeAsync(string wp0, string wp1)
        {
            var client = new HttpClient();
            BingAPIResourceSets result;
            using (var response = await client.GetAsync($"http://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0={wp0}&wp.1={wp1}&avoid=minimizeTolls&key=ArmX4dOZmR9EIMq4rEwgV8yluMld81wDC8zqdcik341GRzP_6jcoWMIHkfsfvFsR"))
            {
                var responseAsString = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<BingAPIResourceSets>(responseAsString);
            }
            return result.resourceSets.
                  Select(res => res.resources).First().Select(res => res.travelDuration).First();

        }
    }
}
