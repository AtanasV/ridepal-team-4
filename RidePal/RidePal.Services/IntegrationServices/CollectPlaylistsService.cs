﻿using Newtonsoft.Json;
using RidePal.Data.Models.DeezerAPIMappers;
using RidePal.Data.Models.DeezerAPIModels;
using RidePal.Services.Contracts;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RidePal.Services.IntegrationServices
{
    public class CollectPlaylistsService : ICollectPlaylistsService
    {
        /// <summary>
        /// Returns collection of playlist ids, taken from Deezer API's top charts for the following genres: Rap/Hip Hop, Rock, Metal, Classical, Jazz.
        /// </summary>
        /// <returns>Collection of playlists' ids.</returns>
        public async Task<List<long>> GetPlaylistsFromChartsAsync()
        {
            var client = new HttpClient();
            DeezerAPIChartData result;
            List<long> playlistNum = new List<long>();
            int counter = 0;
            var collection = new List<int> { 116, 152, 464, 98, 129 };
            for (int i = 0; i < collection.Count; i++)
            {
                using (var response = await client.GetAsync($"https://api.deezer.com/chart/{collection[i]}/playlists"))
                {
                    var responseAsString = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<DeezerAPIChartData>(responseAsString);
                    foreach (var item in result.data)
                    {
                        playlistNum.Add(item.GetModel());
                        counter++;
                        if (counter % 45 == 0)
                        {
                            Thread.Sleep(5000);
                        }
                    }
                }
            }
            return playlistNum;
        }
    }
}
