﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RidePal.Services.Contracts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RidePal.Services.IntegrationServices
{
    public class MyHostedService : IHostedService
    {
        private Timer timer;
        private readonly IServiceProvider serviceProvider;

        public MyHostedService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.timer = new Timer(CallSyncGenresAsync, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(60));

            return Task.CompletedTask;
        }

        private async void CallSyncGenresAsync(object state)
        {
            using (var scope = this.serviceProvider.CreateScope())
            {
                var syncGenresService = scope.ServiceProvider.GetRequiredService<ISyncGenresService>();
                await syncGenresService.SyncGenresAsync();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
