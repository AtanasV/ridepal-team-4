﻿using Newtonsoft.Json;
using RidePal.Data.Context;
using RidePal.Data.Models.DeezerAPIMappers;
using RidePal.Data.Models.DeezerAPIModels;
using RidePal.Services.Contracts;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RidePal.Services.IntegrationServices
{
    public class SeedService : ISeedService
    {
        private readonly RidePalContext dbContext;
        private readonly ICollectPlaylistsService collectPlaylistsService;

        public SeedService(RidePalContext dbContext, ICollectPlaylistsService collectPlaylistsService)
        {
            this.dbContext = dbContext;
            this.collectPlaylistsService = collectPlaylistsService ?? throw new ArgumentNullException(nameof(collectPlaylistsService));
        }

        /// <summary>
        /// Fetches tracks, albums, artists from Deezer API by previuously fetched ids of top chart playlists and updates DataBase.
        /// </summary>
        /// <returns>void</returns>
        public async Task GetDataFromExternalAPIAsync()
        {
            var client = new HttpClient();
            DeezerAPIPlaylistData result;
            var collection = await collectPlaylistsService.GetPlaylistsFromChartsAsync();
            for (int k = 0; k < collection.Count; k++)
            {
                Thread.Sleep(150);
                using (var response = await client.GetAsync($"https://api.deezer.com/playlist/{collection[k]}/tracks"))
                {
                    var responseAsString = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<DeezerAPIPlaylistData>(responseAsString);
                }
                await SeedDataAsync(result);
            }
        }

        /// <summary>
        /// Updates DataBase with tracks, albums, artists, genres extracted from the input collection of tracks.
        /// </summary>
        /// <param name="result">Collection of tracks previously fetched from Deezer API.</param>
        /// <returns>void</returns>
        private async Task SeedDataAsync(DeezerAPIPlaylistData result)
        {
            int index = 0;
            foreach (var item in result.data)
            {
                try
                {
                    await FillGenreAsync(item);
                    var genre = item.album.genre.GetModel();
                    var artist = item.artist.GetModel();
                    var album = item.album.GetModel();
                    var track = item.GetModel();

                    if (!dbContext.Genres.Any(g => g.Name == genre.Name))
                    {
                        dbContext.Genres.Add(genre);
                        dbContext.SaveChanges();
                    }
                    if (!this.dbContext.Artists.Any(a => a.Name == artist.Name))
                    {
                        dbContext.Artists.Add(artist);
                        dbContext.SaveChanges();
                    }
                    if (!this.dbContext.Albums.Any(al => al.Name == album.Name))
                    {
                        album.GenreId = dbContext.Genres.First(genre => genre.DeezerAPIId == item.album.genre.id).Id;

                        dbContext.Albums.Add(album);
                        dbContext.SaveChanges();
                    }
                    if (!this.dbContext.Tracks.Any(t => t.Title == track.Title))
                    {
                        track.AlbumId = dbContext.Albums
                            .First(album => album.DeezerAPIId == item.album.id).Id;

                        track.ArtistId = dbContext.Artists
                            .First(artist => artist.DeezerAPIId == item.artist.id).Id;

                        dbContext.Tracks.Add(track);
                        dbContext.SaveChanges();
                    }

                    index++;

                    if (index % 45 == 0)
                    {
                        Thread.Sleep(5000);
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        /// <summary>
        /// Updates DataBase with genres, fetched from the input track's album id.
        /// </summary>
        /// <param name="track">Track previously fetched from Deezer API.</param>
        /// <returns>void</returns>
        private async Task FillGenreAsync(DeezerAPITrack track)
        {
            var album = track.album;
            var client = new HttpClient();
            var id = album.id;
            using (var extendedAlbum = await client.GetAsync($"http://api.deezer.com/album/{id}"))
            {
                var responseAsString = await extendedAlbum.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<DeezerAPIExtendedAlbum>(responseAsString);
                if (result.genres == null)
                {
                    throw new ArgumentNullException();
                }
                track.album.genre = result.genres.data.First();
            }
        }
    }
}
