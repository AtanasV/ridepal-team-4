﻿using Newtonsoft.Json;
using RidePal.Data.Context;
using RidePal.Data.Models.DeezerAPIMappers;
using RidePal.Data.Models.DeezerAPIModels;
using RidePal.Services.Contracts;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RidePal.Services.IntegrationServices
{
    public class SyncGenresService : ISyncGenresService
    {
        private readonly RidePalContext dbContext;

        public SyncGenresService(RidePalContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Sinchronizes the genres in DataBase with the Deezer API by adding all the new genres in DB.
        /// </summary>
        /// <returns>void</returns>
        public async Task SyncGenresAsync()
        {
            var client = new HttpClient();
            DeezerAPIGenresData result;
            using (var response = await client.GetAsync($"https://api.deezer.com/genre"))
            {
                var responseAsString = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<DeezerAPIGenresData>(responseAsString);
            }

            foreach (var item in result.data)
            {
                if (!dbContext.Genres.Any(g => g.Name == item.name))
                {
                    var genreToAdd = item.GetModel();
                    dbContext.Genres.Add(genreToAdd);
                    dbContext.SaveChanges();
                }
            }
            Console.WriteLine("Syncing Genres");
        }
    }
}
